import React from 'react'

const ExerciseForm = ({ onChange, onSubmit, form }) => (
<div className="container">
        <form 
            onSubmit={onSubmit}
        >
            <div className="form-group">
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="title" 
                    name="title"
                    onChange={onChange}
                    value={form.title}
                />
            </div>
            <div className="form-group">
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="description" 
                    name="description"
                    onChange={onChange}
                    value={form.description}
                />
            </div>
            <div className="form-group">
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="img" 
                    name="img"
                    onChange={onChange}
                    value={form.img}
                />
            </div>
            <div className="form-row">
                <div className="col">
                    <input 
                        type="text" 
                        className="form-control" 
                        placeholder="leftColor" 
                        name="leftColor"
                        onChange={onChange}
                        value={form.leftColor}
                    />
                </div>
                <div className="col">
                    <input 
                        type="text" 
                        className="form-control"
                        placeholder="rightColor" 
                        name="rightColor"
                        onChange={onChange}
                        value={form.rightColor}
                    />    
                </div>
            </div>
            
            <button 
                type="submit" 
                className="btn btn-primary float-right"
            >
                Submit
            </button>
        </form>
    </div>
)

export default ExerciseForm

/*state = {}

    handleOnSubmit = (e) => {
        e.preventDefault()
        console.log(this.state);
    }

    handleOnChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render(){
        return(
            <div className="container">
                <form onSubmit={this.handleOnSubmit}>
                    <div className="form-group">
                        <input 
                            type="email"
                            className="form-control"
                            placeholder="title"
                            name="title"
                            onChange={this.handleOnChange}
                            value={this.state.title}
                        />
                    </div>
                    <div className="form-group">
                    <input 
                            type="text"
                            className="form-control"
                            placeholder="description"
                            name="description"
                            onChange={this.handleOnChange}
                            value={this.state.description}
                        />
                    </div>
                    <div className="form-group">
                    <input 
                            type="text"
                            className="form-control"
                            placeholder="img"
                            name="img"
                            onChange={this.handleOnChange}
                            value={this.state.img}
                        />
                    </div>
                    <div className="form-row">
                        <div className="col">
                            <input 
                                type="text" 
                                className="form-control" 
                                placeholder="leftColor" 
                                name="leftColor"
                                onChange={this.handleOnChange}
                                value={this.state.leftColor}
                            />
                        </div>
                        <div className="col">
                        <input 
                            type="text" 
                            className="form-control"
                            placeholder="rightColor" 
                            name="rightColor"
                            onChange={this.handleOnChange}
                            value={this.state.rightColor}
                        />    
                        </div>
                    </div>
                    <button 
                        type="submit" 
                        className="btn btn-primary float-right"
                    >
                        Submit
                    </button>
                </form>
            </div>
        )
    }*/