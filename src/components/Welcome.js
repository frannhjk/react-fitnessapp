import React from 'react';

import './styles/Welcome.css'

const Welcome = (props) => (
    <div className="container">
        <div className="Fitness-User-Info">
            <h1>Hola {props.userName}!</h1>
            <p>Vamos a trabajar!</p>
        </div>
    </div>
)

export default Welcome