import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import 'bootstrap/dist/css/bootstrap.css'


function toStringName(user){
  return user.firstName + ' ' + user.lastName;
}

const user = {
  firstName: "Franco",
  lastName: "Dugo"
};

const element = <h1>Hola {toStringName(user)}</h1>
const container = document.getElementById('root')

ReactDOM.render(<App />, container)