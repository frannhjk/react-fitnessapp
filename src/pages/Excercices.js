import React, { Fragment } from 'react';

//import fakeJsonData from '../fakeData.json';

// Components
import Welcome from '../components/Welcome'
import ExerciseList from '../components/ExerciseList'
import AddButton from '../components/Button'
import Loading from '../components/Loading'
import FatalError  from './500';


// http://localhost:8000/api/exercises

class Exercices extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            data: [],
            loading: true,
            error: null
        }
    }

    async componentDidMount(){
        await  this.fetchExercises()
    }

    fetchExercises = async () => {
        try{
            var res = await fetch('http://localhost:8000/api/exercises')
            var data = await res.json()
    
            this.setState({
                data,
                loading: false
            })
    
            console.log(data)
        }
        catch(error){
            this.setState({
                loading: false,
                error
            })
        }
    }

    render(){
        if (this.state.loading)
            return <Loading />
        if (this.state.error)
            return <FatalError />
        return(
            <Fragment>
                <Welcome 
                    userName="Franco"
                />
                <ExerciseList 
                    exercises={this.state.data}
                />
                <AddButton
                />
            </Fragment>
        )
    }
}

export default Exercices;