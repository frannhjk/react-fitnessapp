import React from 'react'

import ExerciseForm from '../components/ExerciseForm'
import Card from '../components/Card'
import FatalError from './500';

class ExercicesNew extends React.Component{


    state = {
        form: {
            title: '', 
            description: '', 
            img: '', 
            leftColor: '', 
            rightColor: ''
        },
        loading: false,
        error: null
    }

    handleOnSubmit = async (e) => {
        this.setState({
            loading: true
        })
        e.preventDefault()
        try{
            var config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(this.state.form)
            }
            var response = await fetch('http://localhost:8000/api/exercises', config)
            var json = await response.json();
            console.log(json)

            this.setState({
                loading: false
            })

            // Reedirección
            this.props.history.push('/exercise') 
        }
        catch(error){
            console.log(error);
            this.setState({
                loading: false,
                error: error
            })
        }
    }

    handleOnChange = (e) => {
        this.setState({
            form:{
                ...this.state.form,
                [e.target.name]: e.target.value
            }
            
        })
    }

    render(){
        if (this.state.error)
            return <FatalError />
        return (
            <div className="row">
                <div className="col-sm">
                    <Card {...this.state.form}/>
                </div>
                <div className="col-sm">
                <ExerciseForm
                    onChange={this.handleOnChange}
                    onSubmit={this.handleOnSubmit}
                    form={this.state}
                />
                </div>
            </div>
        )
    }
    
}

export default ExercicesNew