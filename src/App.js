import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import ExercicesPage from './pages/Excercices'
import ExercicesNewPage from './pages/ExercicesNew'
import NotFound from './pages/NotFound'

const App = () => (
    <BrowserRouter>
        <Switch>
            <Route path="/exercise/new" component={ExercicesNewPage} />
            <Route path="/exercise" component={ExercicesPage} />
            <Route component={NotFound} />
        </Switch>
    </BrowserRouter>    
)

export default App
 
